import functools
import gzip
import json
import os
import pandas as pd
import random
import re
import shutil
import subprocess
import sys
import time
import urllib
from scipy.cluster.hierarchy import linkage,dendrogram,cut_tree,fcluster
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.decomposition import non_negative_factorization as nmf
import matplotlib.pyplot as plt
import numpy as np
import functools
from IPython.display import clear_output

from math import sqrt
import scipy.spatial.distance as ssd
from scipy.stats.mstats import gmean 

with open('/home/skw24/Bacteria/bac_extra.txt','r') as f:
    bac_extra = f.read().split('\n')

# results are relative to the calling path    
global_results_path = lambda string: os.path.join(os.getcwd(),'results',string)
# res needs changing to wherever the results will be stored
res = lambda string: os.path.join(
    '/its/home/skw24/Bacteria/results1',string)

def normalise(s):
    return s/s.sum()

def normalised_nmf(df,n_components):
 
    '''Given a pd.DataFrame and number of signatures into which to decompose it, uses the scipy functions to decompose.
    The added value is that the basis is normalised and the error provided. 
    Returns W,H,V, error'''

    V = df.apply(normalise).T.values
    W0,H0,_ = nmf(V,n_components = n_components,init = 'nndsvd')
    HT = pd.DataFrame(H0.T,index = quadruplets_sorted)
    Hsizes = HT.apply(lambda col: sqrt((col**2).sum()))
    W = pd.DataFrame(W0)
    W*=Hsizes
    H=(HT/Hsizes).T
    error = ((V-np.matmul(W.values,H.values))**2).sum()
    return W,H,V,error

four_fold_redundancy = frozenset([i+j for i in ['AC','GT','GG','GC','TC','CT','CC','CG'] for j in 'ACGT'])


def plot_quads(series, short = False,xlabel = '',ylabel = '',ax = False):
    """plots 96 dimensional series according to mutational signature
    format"""
    if not ax:
        fig,ax = plt.subplots()
    params = {False:(6,['C>A','C>G','C>T','T>A','T>C','T>G']), 
              True:(4,['C>A','C>G','C>T','T>A'])}

    colors = []
    for c in ['blue','black','red','gray','green','pink']:
        for i in range(16):
            colors.append(c)
    ax= series.plot(kind = 'bar',ax = ax, color = colors)
    ax.set_xticks([i*16+8 for i in range(params[short][0])])
    ax.set_xticklabels(params[short][1],rotation = 0);
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

quadruplets =pd.Series([a+b+c+d for a in 'ACGT' for b in 'CT' for c in 'ACGT' for d in 'ACGT' if b!=c])
quadruplets_sorted = quadruplets[quadruplets.map(lambda x: x[1:3]+x[0]+x[3]).sort_values().index].values
all_quads = set(quadruplets)

def ct(x):
    '''helper function for Fingerprints - reverses the order of read of mutation if not CT'''
    try:
        comp = {'A':'T','C':'G','G':'C','T':'A'}
        if x[1] in 'CT':
            return x
        else:
            y = [comp[i] for i in x]
            return y[3]+y[1]+y[2]+y[0]
    except (KeyError,IndexError):
        return ''

class BacterialGenome:
    '''Once initiated with bacteria_name, 
    BacterialGenome then retrieves all the urls
    that point to the correct bacteria.   
    Public function - download_next, downloads the cdna data from the next url and updates the index.
    '''
  
    
    url = 'ftp://ftp.ensemblgenomes.org/pub/bacteria/current/fasta'
    
    
    def _get_text(url):
        '''returns a list of text from a url decoded by urrlib so that it copes with ftp sites'''
        req = urllib.request.urlopen(url)
        text = req.read().decode().split('\n')[:-1]
        req.close()
        return text

    def __init__(self,bacteria_name,directories):
        
        self.directories = directories
        self.bacteria_name = bacteria_name
        self.data_path= '/home/skw24/Bacteria/{}/data'.format(bacteria_name)
        self.paths,self._all_paths = self._get_paths()
        self.path_index = 0
    
    def _get_paths(self):
        if 'paths.txt' in os.listdir(self.directories.basic):
            with open(os.path.join(self.directories.basic,'paths.txt')) as f:
                paths = f.read().split('\n')
            
        else:

            text1 = BacterialGenome._get_text(BacterialGenome.url)
            names = [os.path.join(BacterialGenome.url,i.split(' ')[-1][:-1]) for i in text1]
            paths = []
            print('identifying all the urls that have our bacteria in the current release')
            L = len(names)
            percentages = []
            for i,name in enumerate(names):
                text = BacterialGenome._get_text(name)
                new_paths = [os.path.join(name,i.split(' ')[-1][:-1],'cdna') for i in text]
                paths+=new_paths
                j = int(i/L*100)
                if j not in percentages:
                    print(j,' ',end = '')
                    percentages.append(j)
            random.shuffle(paths)
            with open(os.path.join(self.directories.basic,'paths.txt'),'w') as f:
                f.write('\n'.join(paths))
        
        fullnames = [i for i in paths if self.bacteria_name in i]
        return fullnames,paths

    def get_file(self,i=0, url = ''):
        '''identifies a url from self.paths and downloads the page using wget.
        Unzips the resulting file and saves it, removing temporary files.'''
        time.sleep(5)
        if not url:
            url = self.paths[self.path_index]
        if i<10:
            try:
                
                s = subprocess.Popen(['wget',
                                      '-m',
                                      url,
                                     ])
                s.communicate()
                path = os.path.join(os.getcwd(),url.split('//')[1])
                listdir = os.listdir(path)
                if 'cdna' in listdir:
                    path = os.path.join(path,'cdna')
                listdir = os.listdir(path)
                
                name_of_file = [i for i in os.listdir(path) if self.bacteria_name in i.lower()][0]
                                
                path_to_file = os.path.join(path,name_of_file)
                try:
                    with gzip.open(path_to_file) as f:
                        g = f.read()
                    name = url.split('/')[-2].lower()
                    gz_stripped_name = '{}.cdna.all.fa'.format(name)

                    self.current_file = os.path.join(self.data_path,gz_stripped_name)
                    with open(self.current_file,'wb') as h:
                        h.write(g)

                except OSError as e:
                    if e.args[0] == "Not a gzipped file (b'>K')":                    
                        with open(path_to_file) as f:
                            g = f.read()
                    name = url.split('/')[-2].lower()
                    gz_stripped_name = '{}.cdna.all.fa'.format(name)

                    self.current_file = os.path.join(self.data_path,gz_stripped_name)
                    with open(self.current_file,'w') as h:
                        h.write(g)
                    

                for_emptying = path[:path.index('/cdna')]
                shutil.rmtree(for_emptying)
                s.communicate()  

            except urllib.error.URLError:
                print('getting a URLError i= '.format(i))
                time.sleep(5)
                i+=1
                self.get_file(i=i)
                
    def download_next(self,verbose = False):

        self.get_file()
        self.path_index+=1

        
        
        
def read(path):
    with open(path) as f:
        g = f.read()
    return g

def write(path,text):
    with open(path,'w') as f:
        f.write(text)

class Directories:
    '''sets up the initial directories needed to carry out the pipeline'''

    def __init__(self,bacteria_name, results = 'results1'):
        self.basic = '/home/skw24/Bacteria/' 
        self.root = os.path.join(self.basic,bacteria_name)
        self.data_dire = os.path.join(self.root,'data')
        self.search_dire = os.path.join(self.root,'searches')
        self.aa_dire = os.path.join(self.root,'aa_fastas1')
        self.analysis_dire = os.path.join(self.root,'analysis')
        self.linclust_dire = os.path.join(self.root,'linclust1')
        self.results_dire = os.path.join(self.root,results)
        self.temp_results_dire = os.path.join(self.results_dire,'temp_um')

        for path in [self.root,self.data_dire,self.search_dire,
                     self.analysis_dire,self.linclust_dire,self.results_dire,self.aa_dire,self.temp_results_dire]:
            if not os.path.isdir(path):
                os.mkdir(path)
                
    def get_path(self):
        directory_list = os.listdir(self.data_dire)
        length = len(directory_list)
        i=0
        while i<length:
            yield os.path.join(self.data_dire,directory_list[i])
            i+=1

def download(bacteria_name,directories):

    """Uses BacterialGenome to download 200 strains of the given
    bacteria name"""
    bg = BacterialGenome(bacteria_name,directories)
    i = 0
    while len(os.listdir(directories.data_dire))<200:
        print(len(os.listdir(directories.data_dire)))
        try:
            bg.download_next()
            print(i,' ', end = '')
            i+=1
        except OSError:
            i+=1
            print('download error',i)
        finally:
            if i> len(bg.paths):
                break
                
def GC_mean(bacname,directories):
    """finds the average GC content"""
    files = os.listdir(directories.data_dire)
    paths = [os.path.join(directories.data_dire,file) 
             for file in files] 

    def get_GC(p):
        with open(p) as f:
            fa = f.read()
        seq = ''.join(fa.split('\n')[1:])
        return (seq.count('G')+seq.count('C'))/len(seq)


    GC = gmean([get_GC(p) for p in paths])
    path = global_results_path('GC_{}.txt'.format(bacname))
    with open(path,'w') as f:
        f.write(str(GC))
        
def get_gene_distribution(cl,bacname,directories):
    """Given a cluster of genes from different strains finds out
    the frequency distribution of the genes across the different
    strains"""
    
    def include200(x):
        if x in (200,201):
            return 200
        else:
            return x
    lengths = [include200(len(i)) for i in cl]
    df = pd.DataFrame(pd.Series([i//10*5 for i in lengths]
                              ).value_counts())
    df.columns = ['value_counts']
    df['ave_length'] = df.index+2.5
    df = df.reindex(df.index.sort_values())
    df['% strains']= df['value_counts']*df['ave_length']
    df['% strains']*=100/df['% strains'].sum()

    s = df.reindex(range(0,100,5))['% strains']

    s.index = s.index.map(lambda x: '{}-{}%'.format(x,x+5))
    
    s.to_csv(os.path.join(directories.results_dire,
                      'gene_dist.csv'), header = False)

    fig,ax = plt.subplots(figsize = (4,3))
    s.plot(kind = 'bar',ax = ax)
    ax.set_xlabel('% strains {}'.format(bacname).replace('_',' '))
    ax.set_ylabel('% genes')
    plt.tight_layout()
    fig.savefig(os.path.join(directories.results_dire,
                             '{}gene_dist.pdf'.format(bacname)))
    
                     
def space_out(x):
    '''given a string takes out the returns (ie \n), replaces - and then replaces
    the returns every 60 characters'''
    stripped = x.replace('\n','').replace('-','')
    length = len(stripped)//60
    return '\n'.join([stripped[i*60:(i+1)*60] for i in range(length+int(len(stripped)%60!=0))])+'\n'

def easy_linclust(file_path,dire, communicate = True):
    '''convenience function to use mmseqs easy-linclust'''
    
    if not os.path.isdir(dire):
        os.mkdir(dire)
    
    outp = os.path.join(dire,'results')
    temp = os.path.join(dire,'temp')
    p = subprocess.Popen(['mmseqs','easy-linclust',file_path,outp,temp])
    if communicate:
        p.communicate()



def get_fastas(path):
    fastas = ''
    names = []
    count = 0

    with open(path) as f:
        while True:
            line = f.readline()
            if line[0]=='>':
                try:
                    name = line.split('|')[1][:-1]
                    if name not in names:
                        count+=1
                        if count<n+1:
                            names.append(name)
                        else:
                            break
                    fastas+=line    

                except IndexError:
                    print(line)
                    go = False
            else:
                fastas+=line
    return fastas,names

def translate(string):
    dic ={'AAA': 'K', 'AAC': 'N', 'AAG': 'K', 'AAT': 'N', 'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T', 'AGA': 'R',
    'AGC': 'S', 'AGG': 'R', 'AGT': 'S', 'ATA': 'I', 'ATC': 'I', 'ATG': 'M', 'ATT': 'I', 'CAA': 'Q', 'CAC': 'H', 'CAG': 'Q',
    'CAT': 'H', 'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P', 'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R', 'CTA': 'L',
    'CTC': 'L', 'CTG': 'L', 'CTT': 'L', 'GAA': 'E', 'GAC': 'D', 'GAG': 'E', 'GAT': 'D', 'GCA': 'A', 'GCC': 'A', 'GCG': 'A',
    'GCT': 'A', 'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G', 'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V', 'TAA': '*',
    'TAC': 'Y', 'TAG': '*', 'TAT': 'Y', 'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S', 'TGA': '*', 'TGC': 'C', 'TGG': 'W',
    'TGT': 'C', 'TTA': 'L', 'TTC': 'F', 'TTG': 'L', 'TTT': 'F'}
    a=string.upper()
    a.replace('U','T')
    a.replace('\n','')
    
    return ''.join([dic.get(a[3*i:3*(i+1)],'X') for i in range(len(a)//3)])

def aa_fasta(fasta,file):
    'translate a single fasta'
    
    split = fasta.split('\n')
    if '.cdna.all.fa' in file:
        file = file[:file.index('.cdna.all.fa')]
    header = file+'|'+split[0].split(' ')[0]
    seq = ''.join(split[1:])
    aa_seq = translate(seq)
    L=len(aa_seq)
    aa_sequence= '\n'.join([aa_seq[i*60:(i+1)*60] for i in range(L//60+(L%60!=0)*1)])[:-1]+'\n'
    return '>{}\n{}'.format(header,aa_sequence)


def get_aa_fastas(nuc_fastas,file):
    'translate string of nucleotide fastas'
    
    return ''.join([aa_fasta(i,file) for i in nuc_fastas.split('>')[1:]])

def translate_from_directory(directories):
    
    path_gen = directories.get_path()
    counter = 0
    while True:
        counter+=1
        try:
            path = next(path_gen)
            with open(path) as f:
                text = f.read()
            aas = get_aa_fastas(text, path.split('/')[-1])
            aa_path = path.replace('data','aa_fastas1')
            with open(aa_path,'w') as f:
                f.write(aas)
            print(counter,' ', end = '')
        except StopIteration:
            break



class TwoHundred:
    
    '''creates the consensus sequences from the first 100 bacterial strains found.
    In turn it: translates the nucleotides to aas and saves them, carries out an mmseqs linclust to cluster the genes, then uses clustalo to align them, so that a consensus can be reached. 
    '''
   
    
    def __init__(self,bacname,directories):
        
        self.bacname = bacname
        self.directories = directories
        self.make_two_hundred()
        self.large = os.path.join(self.directories.root,'two_hundred.fa')
        self.files = os.listdir(self.directories.aa_dire)[:200]
        self.paths = [os.path.join(self.directories.aa_dire,f) for f in self.files]
        print('two hundred cluster finished')
        nuc_paths = [os.path.join(self.directories.data_dire,p) for p in os.listdir(self.directories.data_dire)]
        self.dict = dict([(path.split('/')[-1][:-12],read(path).split('>')[1:]) for path in nuc_paths])
        
        super_dict_path = os.path.join(self.directories.root, 'superdict.json')
        if not os.path.isfile(super_dict_path):
            value = lambda k: dict([(i.split('\n')[0].split(' ')[0],'\n'.join(i.split('\n')[1:])) for i in self.dict[k]])
            self.super_dict = dict([(k,value(k)) for k in self.dict.keys()])
            with open(super_dict_path,'w') as f:
                json.dump(self.super_dict,f)
        else:
            with open(super_dict_path,'r') as f:
                self.super_dict=json.load(f)

        if not os.path.isfile(os.path.join(self.directories.linclust_dire,'results_cluster.tsv')):
            easy_linclust(self.large,
                          self.directories.linclust_dire)

        self.results_cluster = pd.read_csv(
            os.path.join(self.directories.linclust_dire,
                         'results_cluster.tsv'),
                                      sep = '\t',
                                      header= None)

        clusters_path = os.path.join(
            self.directories.linclust_dire,
            'clusters.json')
        if not os.path.isfile(clusters_path):
            self.clusters = list(pd.Series(
                self.results_cluster.groupby(by = 0).groups).map(
                lambda x: list(frozenset(
                    self.results_cluster[1].reindex(x)))))

            with open(clusters_path,'w') as f:
                json.dump(self.clusters,f)
        else:
            with open(clusters_path) as f:
                self.clusters = json.load(f)
            
        self.good_clusters = self.get_good_clusters()
        
        
    def make_two_hundred(self):

        if not os.path.isfile(os.path.join(self.directories.root,'two_hundred.fa')):

            two_hundred_fa = ''
            files_for_transfer = os.listdir(self.directories.data_dire)
            if len(os.listdir(self.directories.aa_dire))<200:
                i = 0
                print('translating fastas')
            while len(os.listdir(self.directories.aa_dire))<200:
                bg = BacterialGenome(self.bacname,self.directories)
                f = files_for_transfer[i]
                path = os.path.join(self.directories.data_dire,f)
                text = read(path)
                aas = get_aa_fastas(text,f)
                aa_path = path.replace('data','aa_fastas1')
                with open(aa_path,'w') as f:
                    f.write(aas)
                print(i,' ', end = '')
                i+=1

            two_hundred_fa = ''.join([read(os.path.join(self.directories.aa_dire,i)) 
                                      for i in os.listdir(self.directories.aa_dire)])
            write(os.path.join(self.directories.root,'two_hundred.fa'),two_hundred_fa)
       
         
    def get_strain_gene_from_super_dict(self,strain_gene):
        strain,gene = strain_gene.split('|')
        return self.super_dict[strain][gene]
    
    def get_good_clusters(self):
        print('getting good clusters')
        if not os.path.isfile(
            os.path.join(self.directories.results_dire,
                         'good_clusters.json')):
            print('making good clusters')
            c = pd.Series(self.clusters)
            c_shared = c[c.map(lambda x: len(x)==200)]
            c_orthologs = c_shared[c_shared.map(
                lambda x: len(set([i.split('|')[0] for i in x]))
                ==200)]
            good_clusters = list(
                c_orthologs[c_orthologs.map(lambda x: 900< len(
                self.get_strain_gene_from_super_dict(x[0]))<2250)])
            with open(os.path.join(self.directories.results_dire,
                                   'good_clusters.json'),'w'
                     ) as f:
                json.dump(good_clusters,f)
            
            
        
        else:
            print('loading good clusters')
            with open(os.path.join(
                self.directories.results_dire,
                'good_clusters.json'),'r') as f:
                good_clusters = json.load(f)
                
        return good_clusters
    
    def get_length_of_strain_clusters(self):
        ''''length_of_strain_clusters.csv' is a series. 
        An i,v pair has the following meaning: 
        v gives for an average bacterial strain 
        the percentage of genes that have orthologs 
        in i% of the bacterial strains. 
        The term '100+' rounds up all those genes with paralogs.'''
        
        
        #number of strains sharing each cluster
        s = pd.Series(self.clusters).map(lambda x: len(x))

        df = pd.DataFrame([s,s]).T
        # putting the lengths into 10 groups 
        df[0] = df[0].map(lambda x: (x-1)//20*10)
        df1 = df.groupby(by = 0).sum()
        df2 = df1.loc[list(range(0,100,10))]
        df2.index = ['{}-{}'.format(i*10,(i+1)*10) for i in range(10)]
        #including the paralogs
        df2.loc['100+'] = df1.loc[[i for i in df1.index if i>90]].sum()
        #turning into percentages
        df3 = df2/df2.sum()*100
        df3.to_csv(os.path.join(self.directories.results_dire,
                               'length_of_strain_clusters.csv'),
                  header = None)

class Error:
    
    opener = 'w'
    num = 0
    
    def __init__(self,data,directories):
        
        path = os.path.join(directories.root,'errors.txt')
        with open(path,Error.opener) as f:
            f.write('error {} \n{}\n'.format(str(Error.num),data))
        
        Error.num+=1
        Error.opener = 'a'
    

class Quads:
    '''given a row in a cluster dataframe this identifies snps.'''
    
    def __init__(self,row,cluster):
        
        self.row = row
        self.string_row = ''.join(self.row)
        self.cluster = cluster

        self.places = self.get_snp_places()
        if len(self.places)>0:
            self.quad_context = list(map(self.get_quads
                                         ,self.places))
        else:
            self.quad_context = []
            
    def get_snp_places(self):
        
        changes = list(self.row[self.row!= self.cluster.best].index)
        if len(changes)>1:
            nearest = list(zip(changes[1:]+[changes[0]],
                               changes,[changes[-1]]+changes[:-1]))
            #take out those closer than 10bps to nearest neighbour
            snp_places =  [i[1] for i in nearest if min(
                abs(i[0]-i[1]),abs(i[1]-i[2]))>9]
            if 0 in snp_places:
                snp_places.remove(0)
            last = self.row.size
            if last in snp_places:
                snp_places.remove(last)
            return snp_places
        else:
            return []
    
    def get_quads(self,i):
        if 0<i<self.row.shape[0]-1:
            snp = self.row[i-1]+self.cluster.best[i]+self.row[i]+self.row[i+1]
            codon = self.string_row[i-2:i+1]
            best_codon = self.cluster.string_best[i-2:i+1]
            return [snp,codon,best_codon,i%3]
        else:
            return ['','','',-1]

class Cluster:
    '''a cluster is a cluster of genes identified by linclust. 
    Properties:
        cluster: list of strain/genes
        data: data for aligning
        good_seqs: dataframe of aligned sequences- 
        removing any indels before the start of the consensus.
        best: the consensus sequence
        string_best: the consensus sequence as a string
        
    Methods:
        quads_not_reduced - returns list of the quads for snps 
    
        count_background(self): returns tuple: 
            all triplets in the consensus sequence;
            just those that could be mutated with fourfold redundancy;
            codon pluses = all codons plus the 3' flanking nucleotide;

    '''
    
    
        
    def __init__(self,cluster,two_hundred,
                 directories,selected_strains=[]):
        
        self.directories = directories
        self.cluster = cluster   
        self.selected_strains = selected_strains
        self.two_hundred = two_hundred
        self.bacname = self.two_hundred.bacname
        self.data = self.get_clustalo_data()
        x=time.time()
        fastas = self.clustalo()
        self.names = [i.split('|')[0] for i in fastas]

        y = time.time()-x
        self.seqs = pd.DataFrame([list(''.join(
            i.split('\n')[1:])) for i in fastas])
        best = self.seqs.loc['best_seq'] = self.seqs.apply(
            lambda col:col.value_counts().index[0])
               
        #take out any indels at the start
        try:
            start = ''.join(best).index('ATG')
        except ValueError:
            print('ATG not present \n'.format(self.cluster))
            start = 0
            
        self.good_seqs = self.seqs.iloc[:,start:]
        self.good_seqs.index = self.names+['best_seq']
        self.good_seqs.columns = range(self.good_seqs.shape[1])
        self.best = self.good_seqs.loc['best_seq']
        self.string_best = ''.join(self.best)
        
        
        s = self.seqs.apply(lambda row: (row == self.seqs.iloc[-1]).astype(int) ,axis = 1)
        self.seq_id_series = (s.T.sum()/s.shape[1])[:-1]
        self.similar = self.seq_id_series[self.seq_id_series>0.99].index
        self.num_similar = self.similar.shape[0]
        self.all_names = pd.Series(self.cluster).map(lambda x:x.split('|')[0])
        self.names = list(set(self.all_names[self.similar]))
        
    def get_clustalo_data(self):
        '''a cluster is a list of strain|genes that were identified by linclust as being related'''
        for_clustalo = ''
        for j in self.cluster:
            strain,gene = j.split('|')
            if strain in self.two_hundred.super_dict.keys():
                if gene in self.two_hundred.super_dict[strain].keys():
                    for_clustalo+='>'+j+'\n'+self.two_hundred.super_dict[strain][gene]
        return for_clustalo

    def clustalo(self):
        try:
            r = random.randint(100000,999999)
            temp = '/home/skw24/Bacteria/{}/temp{}.fa'.format(self.bacname,str(r))
            temp1 = '/home/skw24/Bacteria/{}/temp{}.fa'.format(self.bacname,str(r+1))
            with open(temp,'w') as f:
                    f.write(self.data)
            p = subprocess.Popen(['clustalo','--force','-i',temp,'-o',temp1])
            p.communicate()
            with open(temp1,'r') as f:
                fastas = f.read().split('>')[1:]
            os.remove(temp)
            os.remove(temp1)
            return fastas

        except (OSError,FileNotFoundError) as e:
            print(e)
            data = str(e)+'\n'+self.data
            Error(data,self.directories)          
            return []
     
    def quads_not_reduced(self):
        return self.good_seqs.apply(lambda row: Quads(row,self).quad_context,axis = 1)
    
    def count_background(self):
        '''return
            codon pluses = all codons plus the 3' flanking nucleotide
            '''
        length = len(self.string_best)//3-1
        codon_pluses = [self.string_best[3*i:3*i+4] for i in range(length)]
        return codon_pluses
    

    def get_uniques(self):
        '''find the dictionary of silent quadruplets that are unique - ie only only of the strains has been
        mutated
        '''
        #initialise
        unique_strain_quad = pd.DataFrame(0,index = quadruplets_sorted,columns = self.selected_strains)
        changed_quads = pd.Series(0,index = quadruplets_sorted)

        width = self.good_seqs.shape[1]
        
        for i in range(1,width//3):
            j=3*i #iterate over the 2nd registers
            if j<width-1: #dont bother with the edges - no good quads
                #check that you're not in a patch of indels
                if set(self.good_seqs.loc['best_seq'][[j-2,j-1,j,j+1]])-set('ACGT')==set():
                    vc= self.good_seqs.iloc[:,j].value_counts()
                    if 1 in set(vc.values):
                        um = vc[vc==1].index  #if any of the snps are unique    
                        for m in um:#iterated through unique snps
                            unique_strain = self.good_seqs[j].loc[self.good_seqs[j]==m].index[0]
                            snippet = self.good_seqs.loc[[unique_strain,'best_seq']][[j-2,j-1,j,j+1]]
                            if translate(''.join(snippet.iloc[0,:-1]))==translate(''.join(snippet.iloc[1,:-1])):
                                quad = ct(''.join([snippet.iloc[i,j] for i,j in [(1,1),(0,2),(1,2),(1,3)]]))
                                if snippet.iloc[0,2] in set('ACGT'):#check not a deletion
                                    unique_strain_quad.at[quad,unique_strain]+=1
                                    changed_quads[quad]+=1

        return unique_strain_quad,changed_quads

def cluster_genes(two_hundred,directories,gene_num = 100):
    '''go through the first gene_num (normally 100) of the 
    clusters and work out the sequence identity of each of the 
    strains with the consensus sequence'''
    cluster_info = []
    print('clustering_genes')
    for i,cluster in enumerate(two_hundred.good_clusters[:gene_num]):
        try:
            self = Cluster(cluster,
                                     two_hundred,directories)
            cluster_info.append(self)
            print(i, ' ', end = '')
            
        except ValueError as e:
            print(e)
            Error('{}\ncluster_number{}\n{}'.format(str(e),
                                                    i,
                                                    '\n'.join(
                                                        cluster)))
    try:
        with open(os.path.join(directories.temp_results_dire,'cluster_info.json'),'w') as f:
            json.dump(cluster_info,f)
    except:
        pass

            
    return cluster_info

def cluster_strains(cluster_info, bacname, directories, color_threshold = ''):
    '''cluster_info is a list of Clusters (object formed from cluster of genes       identified by linclust, more info in bacteria2.py). Uses their sequence           identities to form a linkage matrix. '''
    
    similarities = []
    for self in cluster_info:
        self.seq_id_series.index  =self.all_names
        similarities.append(self.seq_id_series)

    similarities_df = pd.concat(similarities,axis = 1,sort=True)
    distances = (1-similarities_df).fillna(1)
    distances.to_csv(os.path.join(
        directories.results_dire,'distances.csv'))

    Z = linkage(distances,method = 'ward')
    fig,ax = plt.subplots(figsize = (7,3))
    if color_threshold == '':
        color_threshold = 0.7
    den = dendrogram(Z,ax=ax, get_leaves = True, 
                     no_labels = True)

    strain_cluster = pd.DataFrame(pd.Series(fcluster(Z,color_threshold,
                                        criterion = 'distance'),
                               index = distances.index))

    groups = pd.Series(strain_cluster.groupby(by = [0]).groups).map(
        lambda x: list(x))
    numbers = groups.map(len)
    st_clusters = groups.to_dict()

    #save everything
    figpath = os.path.join(directories.results_dire,
                        'clustering_{}_by_sequence_identity.pdf'.format(bacname))
    fig.savefig(figpath)
    np.save(os.path.join(directories.results_dire,'Z.npy'),Z)
    with open(os.path.join(directories.results_dire,
                           'strain_clusters.json')
              ,'w') as f:
        json.dump(st_clusters,f)
    numbers.to_csv(os.path.join(directories.results_dire,
                                'numbers.csv'),
                  header = False)
    return Z,den,fig,st_clusters,numbers

class StrainCluster:
    
    def __init__(self,selected_strains,two_hundred,label,directories,cutoff = 20,show_freq = 10,show = False, force_setup = False):
        self.display = show
        self.text = ''
        self.selected_strains = selected_strains
        self.two_hundred=two_hundred
        self.bacname = self.two_hundred.bacname
        self.label = label
        self.directories = directories
        self.cutoff = cutoff
        self.show_freq = show_freq
        
        self.shared_clusters = self.get_shared_clusters()
        self.length = len(self.shared_clusters)

        temp_path = lambda string: os.path.join(self.directories.temp_results_dire,
                                        '{}_{}_'.format(self.label,self.bacname)+string)
        

        self.setup = True
        if not force_setup:
            
    
            if os.path.isfile(temp_path('label.txt')):
                print('loading')
                self.setup = False
                self.load()
                
        if self.setup:
            self.do_setup()
            
    def do_setup(self):
        print('initialising')
        self._unique_strain_quads = pd.DataFrame(0,index = quadruplets_sorted,columns = self.selected_strains)
        self._unique_quads = pd.Series(0,index = quadruplets_sorted)
        self._all_quads_not_reduced = pd.Series(index = self.selected_strains).map(lambda x:[])
        self._codon_pluses = []
        self.start = 0
       
    def res_path(self,x):
        return os.path.join(self.directories.results_dire,'{}_{}'.format(self.label,x))

    def get_shared_clusters(self):
        '''filters two_hundred.clusters for the selected_strains, pull out any with paralogues, and returns
            gene clusters that have all the selected strains in them'''

        path = self.res_path('shared_clusters_large.json')
        
        if not os.path.isfile(path):
            self.text+='finding shared clusters'
     
            in_selected = lambda cl: [i for i in cl if i.split('|')[0] in self.selected_strains]
            clusters_in_selected = pd.Series(self.two_hundred.clusters).map(in_selected)

            def check_no_paralogues(cl):
                names = [i.split('|')[0] for i in cl]
                return len(names)==len(set(names))

            no_paralogues = clusters_in_selected[clusters_in_selected.map(check_no_paralogues)]
            lengths = no_paralogues.map(len)
            shared_non_paralogous_cluster_index = lengths[lengths==len(self.selected_strains)].index
            shared_clusters= list(clusters_in_selected.reindex(shared_non_paralogous_cluster_index))
            with open(path,'w') as f:
                json.dump(shared_clusters,f)
                 
        else:
            with open(path,'r') as f:
                shared_clusters = json.load(f)
                
        return shared_clusters         
    
    def build_quads(self):
        try:
            cluster= Cluster(self.cl,self.two_hundred,self.directories,self.selected_strains)
            usq,q=cluster.get_uniques()
            self._unique_strain_quads+=usq
            self._unique_quads+=q
            self._all_quads_not_reduced+=cluster.quads_not_reduced()
            self._codon_pluses+=cluster.count_background()
        except (KeyboardInterrupt, OSError) as e:
            raise()
        except:
            pass
        
    def count(self,strain):

        def flatten(s):
            r = s.copy()
            df = pd.DataFrame(list(r.index.map(list)),columns = r.index.names)
            r.index = range(r.shape[0])
            df['values'] = r
            return df
        
        try:
            df = pd.DataFrame(self._all_quads_not_reduced[strain],columns = ['snp','codon','best_codon','register'])
            df['corrected'] = df['snp'].map(ct)
            df = df.loc[df['corrected'].map(lambda x: x in all_quads)]
            df = flatten(df.groupby(df.columns.tolist(),as_index=False).size())
            four_fold_redundancy = frozenset([i+j for i in ['AC','GT','GG','GC','TC','CT','CC','CG'] for j in 'ACGT'])

            df['silent'] = df.apply(lambda row: (translate(row['codon'])==translate(row['best_codon']))
                                    and (row['register']==2),
                                    axis=1)

            df['four_fold'] = df.apply(lambda row: (row['codon'] in four_fold_redundancy) and 
                                             (row['register']==2),axis=1)
        
            df['name'] = strain
        except ValueError:
            df = pd.DataFrame(columns = ['snp','codon','best_codon','register',
                                         'corrected','silent','four_fold','name'])
        
        
        return df

    def save(self,finished):
        '''get_quads is a slow laborious method that may fail if the pipe is broken so temporary 
        results need to be saved from time to time. That's what this method does.
        '''
        

        if finished:
            
            temp_path = lambda string: os.path.join(self.directories.results_dire,
                                               '{}_{}_'.format(self.label,self.bacname)+string)
            print(self.start,finished,temp_path)
            
        else:
         
            temp_path = lambda string: os.path.join(self.directories.temp_results_dire,
                                               '{}_{}_'.format(self.label,self.bacname)+string)

            print(self.start,finished,temp_path)

            
        self._unique_strain_quads.to_csv(temp_path('unique_strain_quads.csv'))
        self._unique_quads.to_csv(temp_path('unique_quads.csv'),header = False)
        self._all_quads_not_reduced.map(lambda x:str(x)).to_csv(temp_path('all_quads_not_reduced.csv'),
                                                                header = False)
        with open(temp_path('label.txt'),'w') as f:
            f.write(str(self.label))
        with open(temp_path('codon_pluses.json'),'w') as f:
            json.dump(self._codon_pluses,f)
        with open(temp_path('start.txt'),'w') as f:
            f.write(str(self.start))
            
        if finished:
            self.all_quads_counted = pd.concat(list(self._all_quads_not_reduced.index.map(self.count)),sort = True)
            self.all_quads_counted.to_csv(temp_path('all_quads_counted.csv'))   


            

    def load(self):
        '''get_quads is a slow laborious method that may fail if the pipe is broken so temporary 
        results need to be loaded from time to time. That's what this method does.
        '''
        temp_path = lambda string: os.path.join(self.directories.temp_results_dire,
                                               '{}_{}_'.format(self.label,self.bacname)+string)

        self._unique_strain_quads = pd.read_csv(temp_path('unique_strain_quads.csv'),index_col = 0)
        self._unique_quads = pd.read_csv(temp_path('unique_quads.csv'),header = None,index_col = 0,squeeze = True)
        self._all_quads_not_reduced = pd.read_csv(temp_path('all_quads_not_reduced.csv'),squeeze = True,
                                                  index_col = 0, header = None).map(lambda x: eval(x))
        with open(temp_path('codon_pluses.json'),'r') as f:
            self._codon_pluses = json.load(f)
        with open(temp_path('start.txt'),'r') as f:
            self.start = int(f.read())

    def get_quads(self, number = 0):
        '''The meat of the class. This function iterates through shared genes, calling build quads
        which clusters them and finds all the snps as well as just the unique ones. 
        It also finds the distribution
        of possible silent snps'''
           
        if number>0:
            cluster_slice = self.shared_clusters[self.start:self.start+number]
        else:
            cluster_slice = self.shared_clusters[self.start:]
        
        for self.cl in cluster_slice:
            self.build_quads()           
            print(self.start,' ', end = '')
            self.start+=1
            if (self.show_freq>0) and (self.start%self.show_freq==0):
                if self.display:
                    plot_quads(self._unique_quads)
                    plt.show()
                self.save(False)
        self.save(True)
                
        return    self.all_quads_counted,self._unique_strain_quads

    
    def group_values(self):
        path = self.res_path('grouped_values.csv')
        silent_quads = self.all_quads_counted.loc[
            self.all_quads_counted['silent']]
        silent_quads.index = range(silent_quads.shape[0])
        groups = pd.Series(silent_quads.groupby(by = 'name').groups)

        def make_96(x):
            reindexed = silent_quads.reindex(x)
            series= pd.Series(reindexed['values'].values,index = reindexed['corrected'].values).groupby(level = 0).sum()

            return series.reindex(quadruplets_sorted).fillna(0).astype(int)

        grouped_values = pd.concat(list(groups.map(make_96)),axis = 1)
        grouped_values.columns = groups.index
        grouped_values.to_csv(path)
        return grouped_values
    
 
    def silent_distribution(self):
        '''A codon_plus is the four nucleotides making up a codon and the final nucleotide which flanks the codon.
        This is sufficient information to identify the silent possible quads that could result from a mutation in 
        the 2nd register.

        Input : a list of codon_pluses generated by looking at a nucleotide sequence;
        Output: a series - the distribution of potential silent quadruplets, ordered by mutation first and
        then the flanking nucleotides. '''
        path = self.res_path('distribution_of_random_silent_mutations.csv')

        class SilentCounts:
            silent_quad_counts = {}
            with open('silent_quads.json','r') as f:
                silent_quads = json.load(f)

            def __init__(self,codonplus):
                new_quads = SilentCounts.silent_quads[codonplus]
                value = codon_plus_value_counts[codonplus]
                for quad in new_quads:
                    SilentCounts.silent_quad_counts[quad]=SilentCounts.silent_quad_counts.get(quad,0)+value

        codon_plus_value_counts_rough = pd.Series(self._codon_pluses).value_counts()
        codon_plus_value_counts = codon_plus_value_counts_rough.reindex(
            [i for i in codon_plus_value_counts_rough.index if '-' not in i])

        codon_plus_value_counts.index.map(SilentCounts);

        silent_possibilities_rough =  pd.Series(SilentCounts.silent_quad_counts)
        silent_possibilities_rough.index = silent_possibilities_rough.index.map(ct)
        silent_possibilities = silent_possibilities_rough.groupby(level = 0).sum()[
            quadruplets_sorted].fillna(0).astype(int)
        silent_possibilities.to_csv(path,header  = False)
        return silent_possibilities

    def get_results(self):
        
        path = self.res_path('results.csv')
        
        if not os.path.isfile(path):
            normalised_snps = self.silent_snps/self.silent_snps.sum()
            normalised_possibilities = self.silent_possibilities/self.silent_possibilities.sum()
            mutation_rates = (normalised_snps/normalised_possibilities).fillna(1)
            results = pd.concat([normalised_snps,normalised_possibilities,mutation_rates],axis=1)
            results.columns = ['normalised_snps','normalised_possibilities','mutation_rates']
            results.to_csv(path)
        else:
            results = pd.read_csv(path, index_col = 0)
        
        return results
    
    def show(self):
        fig,axarr = plt.subplots(3,figsize = (7,9))
        for i in range(3):
            col = self.results.columns[i]
            ax = axarr[i]
            plot_quads(self.results[col],ax)
            ax.set_title(col.replace('_',' '));
        plt.tight_layout()
        fig.savefig(self.res_path('results.pdf'))
        
    def get_unique_freqs(self):
        silent_dist = normalise(self.silent_possibilities)
        unique_freqs = self.by_strain_df.apply(lambda col: col/silent_dist).fillna(1)
        fig,ax = plt.subplots()
        plot_quads(normalise(unique_freqs.copy().T.sum()),ax =ax)
        fig.savefig(self.res_path('unique_frequencies.pdf'))
        unique_freqs.to_csv(self.res_path('unique_frequences.csv'))
        return unique_freqs

    def cluster_highly_mutated_uniques(self):
        highly_mutated = self.by_strain_df.T.loc[self.by_strain_df.sum()>self.cutoff].index

        highly_mutated_freqs = self.unique_freqs[highly_mutated]
        if highly_mutated_freqs.shape[1]>1:

            distances = pd.DataFrame(1-cosine_similarity(highly_mutated_freqs.T,highly_mutated_freqs.T))
            length = distances.shape[0]
            if distances.sum().sum()>0:
                columns = pd.Series(highly_mutated)

                for i in range(length):
                    distances.iat[i,i]=0
                    for j in range(i+1,length):
                        distances.iat[i,j] = distances.iat[j,i]

                link = linkage(ssd.squareform(distances))

                fig,ax = plt.subplots(figsize = (10,4))
                Z = dendrogram(link,ax = ax,orientation = 'right')

                name = '_'.join(columns[0].split('_')[:2])

                shortening = ''.join([i[0] for i in self.bacname.split('_')])

                strains =[i.replace(name,shortening) for i in columns.reindex(Z['leaves']).values]

                label = ' '.join(self.bacname.split('_')[:2])

                ax.set_yticklabels(strains)
                ax.set_title(label)
                plt.tight_layout()

                fig.savefig(self.res_path('clustered_highly_mutated_strains.pdf'.format(self.bacname,
                                                                                                  self.label)))
                path = '/home/skw24/Bacteria/results1/{}_{}_clustered_highly_mutated_strains.pdf'.format(self.bacname,
                                                                                                        self.label)
                fig.savefig(path)
        return highly_mutated_freqs

    def find_sigs(self,n_components):
        normalised_unique_freqs = self.unique_freqs.apply(normalise)

        V = normalised_unique_freqs.values
        W0,H0,_ = nmf(V.T,n_components = n_components,init = 'nndsvd')

        H = pd.DataFrame(H0.T,index = quadruplets_sorted)

        Hsizes = H.apply(lambda col: sqrt((col**2).sum()))

        W = pd.DataFrame(W0)

        W*=Hsizes

        H/=Hsizes

        fig1,axarr = plt.subplots(n_components+1,figsize = (7,n_components*3))
        plot_quads(normalised_unique_freqs.T.mean(),ax = axarr[0])
        axarr[0].set_title('{} frequency unique mutations'.format(self.bacname))
        plot_quads(pd.Series(H[0],index = quadruplets_sorted),ax = axarr[1])
        axarr[1].set_title('signature 0')
        plot_quads(pd.Series(H[1],index = quadruplets_sorted),ax = axarr[2])
        axarr[2].set_title('signature 1')
        pd.DataFrame(W).plot(kind = 'bar',ax = axarr[3])
        axarr[3].set_title('breakdown of highly mutated strains by signature')
        plt.tight_layout()

        fig1.savefig(self.res_path('unique_signatures.pdf'))
        fig1.savefig('/home/skw24/Bacteria/results1/{}_{}_unique_signatures.pdf'.format(self.bacname,
                                                                                       self.label))
        return normalised_unique_freqs

    def get_info(self, show_sigs = True):
        self.all_quads_counted,self.by_strain_df = self.get_quads()
        self.silent_possibilities = self.silent_distribution()
        #self.grouped_values = self.group_values()
        self.unique_freqs = self.get_unique_freqs()
        W,H,V,error = normalised_nmf(self.unique_freqs,5)
        H.to_csv(self.res_path('H.csv'))
        if show_sigs:
            self.show_sigs(W,H)
        
    def show_sigs(self,W,H):
        n_components = H.shape[0]
        fig1,axarr = plt.subplots(n_components+2,figsize = (7,n_components*3))
        plot_quads(H.T.mean(),ax = axarr[0])
        axarr[0].set_title('{} frequency unique mutations'.format(self.bacname))
        for i in range(n_components):
            plot_quads(pd.Series(H.iloc[i],index = quadruplets_sorted),
                       ax = axarr[i+1])
            axarr[i+1].set_title('signature {}'.format(i))

        pd.DataFrame(W).plot(kind = 'bar',ax = axarr[-1])
        axarr[-1].set_title('breakdown of highly mutated strains by signature')
        plt.tight_layout()
        fig1.savefig(self.res_path('unique_signatures.pdf'))
        
def do(mylist):
    p= subprocess.Popen(mylist)
    print(p.communicate(), ' '.join(mylist))

def get_ddr_connects(bacname,directories):
    
    query_path = '/its/home/skw24/Bacteria/DDR/ddr_fastas.fa'
    target = os.path.join(directories.root,'two_hundred.fa')
    result = '/its/home/skw24/Bacteria/DDR/{}_ddr_shock.m8'.format(bacname)

    if (os.path.isfile(target)) and (not os.path.isfile(result)):
        tmp = '/its/home/skw24/Bacteria/DDR/temp'
        if os.path.isdir(tmp):
            shutil.rmtree(tmp)

        os.mkdir(tmp)
        do(['mmseqs','easy-search',query_path,target,result,tmp] )
        print('clustering done')
        df = pd.read_csv(result,header = None, index_col = 0, sep = '\t')
        identifier = pd.DataFrame(list(df.iloc[:,0].map(lambda x:x.split('|'))),index = df.index)
        df = pd.concat([identifier,df.iloc[:,1:]],axis=1)
        df.columns = ['strain','gene','seq id','align len','mismatches',
                           'gaps','query start','query end','target start','target end','evalue','bit score']

        df.to_csv(result)
        print(bacname)

        
        
def identifying_data():
    '''identify all those bacteria species with more than 200 strains'''
    
    paths = '/home/skw24/Bacteria/paths.txt'
    with open(paths) as f:
        g = f.read().split('\n')

    strains = [i.split('/')[-2] for i in g]
    names = ['_'.join(i.split('_')[:2]) for i in strains]

    vc = pd.Series(names).value_counts()
    vc_large = vc[vc>200]
    vc_large.to_csv('/home/skw24/Bacteria/good_data.csv', header = False)
    return vc_large


ddr = lambda x: os.path.join('DDR',x)

def download_url(url,directory):
    
    '''download a given named bacteria from a url'''
    
    name = url.split('/')[-2]
    s = subprocess.Popen(['wget',
                              '-m',
                              url,
                             ])
    s.communicate()
    path = os.path.join(os.getcwd(),url.split('//')[1])
    listdir = os.listdir(path)
    if 'cdna' in listdir:
        path = os.path.join(path,'cdna')
    listdir = os.listdir(path)
    name_of_file = [i for i in os.listdir(path) if 
                    name.lower() in i.lower()][0]

    path_to_file = os.path.join(path,name_of_file)
    with gzip.open(path_to_file) as f:
        g = f.read()

    gz_stripped_name = '{}.cdna.all.fa'.format(name)
    current_file = os.path.join(directory,gz_stripped_name)
    with open(current_file,'wb') as h:
        h.write(g)

    shutil.rmtree('/home/skw24/Bacteria/ftp.ensemblgenomes.org')

def make_common_place_ddr():
    '''identifies the fastas for ddr genes in ecoli that 
    are also shared by the other bacteria - will use this to 
    cluster bacteria by'''
    
    standard_ecoli = 'Escherichia_coli.HUSEC2011CHR1'
    url = '''ftp://ftp.ensemblgenomes.org/pub/bacteria/release-47/fasta/bacteria_91_collection/escherichia_coli/cdna/'''

    download_url(url,'')

    with open(os.path.join('DDR',gz_stripped_name)) as f:
        g = f.read().replace('->','-')

    fastas = g.split('>')[1:]

    ecoli_df = convert_fastas_to_df(fastas)

    def get_gene_symbol(x):
        if 'gene_symbol:' in x:
            return x.split('gene_symbol:')[1].split(' ')[0]
        else:
            return np.nan

    ecoli_df['gene_symbol'] = ecoli_df.index.map(get_gene_symbol)

    ecoli_df.dropna(how = 'any',inplace =True)

    ecoli_df_ddr = ecoli_df.loc[ecoli_df['gene_symbol'].map(lambda x:x in genes)]
    ecoli_df_ddr.index = ecoli_df_ddr['gene_symbol']
    ecoli_df_ddr['aas']=ecoli_df_ddr['seq'].map(translate)
    ecoli_df_ddr['aas'] = ecoli_df_ddr['aas'].map(
        lambda a:'\n'.join([a[i:i+60] for i in range(0,len(a),60)]))

    ddr_fastas = ''.join(list('>'+ecoli_df_ddr.index+'\n'+ecoli_df_ddr['aas']+'\n'))

    with open(os.path.join('DDR','commonplace.fa'),'w') as f:
        f.write(ddr_fastas)

def make_targets():
    #firstly make the target databases
    target = os.path.join('DDR','commonplace.fa')
    targetDB = os.path.join('DDR','commonplace')
    tmp = os.path.join('DDR','tmp')
    os.mkdir(os.path.join(tmp))
    do(['mmseqs', 'createdb', target, targetDB])
    do(['mmseqs', 'createindex', targetDB, tmp])
    

def clear_temp():
    'remove the temporary directories'
    if os.path.isdir('DDR/temp'):
        shutil.rmtree('DDR/temp')
    if os.path.isdir('DDR/tmp'):
        shutil.rmtree('DDR/tmp')

def find_matches(query):
    '''use mmseqs to search the query fastas against the
    existing ddr target database saved at targetDB'''
    tmp = os.path.join('DDR','tmp')
    clear_temp()
    os.mkdir(os.path.join('DDR','temp'))
    targetDB = os.path.join('DDR','commonplace')
    queryDB = os.path.join('DDR','temp','queryDB')
    resultDB = os.path.join('DDR','temp','resultDB')
    m8 = os.path.join('DDR','temp','resultDB.m8')
    
    do(['mmseqs', 'createdb',query, queryDB])
    do(['mmseqs', 'search', queryDB, targetDB, resultDB, tmp])
    do(['mmseqs', 'convertalis', queryDB, targetDB,
        resultDB, m8])
               

    df = pd.read_csv('DDR/temp/resultDB.m8',sep = '\t',
                     index_col = 0,header = None)
    
    #pick out the lowest probability matches
    df_no_dups = df.sort_values(
        by = 10).drop_duplicates(subset = 1)
    shutil.rmtree('DDR/temp/')
    matches= df_no_dups[1]
    matches.index = matches.index.map(lambda x: x.split('|')[-1])
    if '' in matches.index:
        matches.drop('',inplace = True)
    return matches
               
               
def short_title_seq(x):
    '''split a fasta into two parts the short title 
    (up to the first space) and the sequence, return as a list'''
    
    xlist = x.split('\n')
    seq = '\n'.join(xlist[1:])
    short_title = xlist[0].split(' ')[0]
    return [short_title,seq]

def add_to_ddr_gene_dict(query,matches,ddr_gene_dict): 
    
    """Given a series of matches and the path to aa fastas
    identify the correct nucleotide fasta, separate off the 
    sequence and add it to the dictionary, returning the 
    dictionary"""
    query_nucs = query.replace('aa_fastas1','data')
    with open(query_nucs) as f:
        query_fastas = f.read().split('>')[1:]

    nuc_seq_df = pd.DataFrame([short_title_seq(x) for 
                               x in query_fastas],
                columns = ['title','seq'])
    nuc_seq_df.index = nuc_seq_df['title']
    nuc_seq_df.drop('title',axis = 1,inplace = True)
    if '' in matches.index:
        matches.drop('',inplace = True)
    ddr_nuc_seq =nuc_seq_df.loc(list(matches.index))
    ddr_nuc_seq.index = matches.values

    for i in ddr_nuc_seq.index:
        ddr_gene_dict[i].append(ddr_nuc_seq.loc[i]['seq'])
    return ddr_gene_dict

def find_ddrs(files,bacname,ddr_gene_dict,url,option):
    """given a url, download the fastas, find matches to the
    ecoli ddr genes and add them to the dictionary, remove
    files."""
    try:
        directories = Directories(bacname)
        download_url(url,directories.data_dire)
        #print('downloaded')
        translate_from_directory(directories)
        #print('translated')
        file = os.listdir(directories.aa_dire)[0]
        query = os.path.join(directories.aa_dire,file)
        matches = find_matches(query)
        #print('matches made')
        try:
            ddr_gene_dict = add_to_ddr_gene_dict(query,
                                                 matches,
                                                 ddr_gene_dict)
            with open(ddr('{}_{}_ddr_gene_dict'.format(option[0],
                                                       bacname)),'w') as f:
                json.dump(ddr_gene_dict,f)
        except Exception as e:
            with open('emergency_gene_dict','w') as f:
                json.dump(ddr_gene_dict,f)
            matches.to_csv('emergency_matches.csv')
            with open('emergency_url.txt','w') as f:
                f.write(url)
            print (e)
            raise
            
            
        #print('genes added and saved')
        files.append(file)
        print(files[-1])
        shutil.rmtree(directories.data_dire)
        shutil.rmtree(directories.aa_dire)
        return files,ddr_gene_dict
    except (pd.errors.EmptyDataError, FileNotFoundError) as e:
        print(e)
        return files,ddr_gene_dict
ddr_genes = ['dinB', 'uvrB', 'rarA', 'mfd', 'topA', 'uvrC', 'ligA', 'ung',           'recN','mutY', 'polA', 'uvrA', 'radA']
    
def get_seqs(bacname,option):
    """identify all the paths, find all the ddr gene
    families and add them to a dictionary,
    saving the dictionary after each new addition.Returns ddr_gene_dict"""

    # initialise paths etc.
    target = os.path.join('DDR','commonplace.fa')
    targetDB = os.path.join('DDR','commonplace')
    ddr_gene_dict = dict([(i,[]) for i in ddr_genes])
    directories = Directories(bacname)
    bg = BacterialGenome(bacname,directories)
    files = []

    with open('{}/results/{}'.format(bacname,option)) as f:
        cl = json.load(f)

    clustered_names = frozenset(
        [i.split('|')[0].split('.')[0].lower() for i in cl[0]])
    urls = [i for i in bg.paths if 
             i.split('/')[-2] in clustered_names]
    print('initialised')
    for i,url in enumerate(urls):
        
        files,ddr_gene_dict = find_ddrs(files,
                                        bacname,
                                        ddr_gene_dict,url,option)
        
        print(pd.Series(ddr_gene_dict).map(len).describe())
        print(i)
        clear_output(wait = True)
        print(bacname)
    return ddr_gene_dict


def snv(row):
    '''identifies all snvs where more than 5% of samples have
    a different allele from the majority'''
    
    v = row.value_counts()
    if '-' in v.index:
        v = v.drop('-')
    if v.shape[0]>1:
        return (v[1:].sum()/v.sum())>0.05
    else:
        return False
    
def is_silent(i,vcs,consensus):
    '''i is the row index at which an allele has been spotted,
    This function tests to see whether the wild and mutant
    amino acid are the same'''
    row = vcs.loc[i]
    mut = row.drop(row.idxmax()).idxmax()
    wildn = consensus[i-2:i+1]
    wilda = translate(wildn)
    mutn = wildn[:-1]+mut
    muta = translate(mutn)
    return muta==wilda

def do_clustalo(s):
        #put all the sequences into a fasta format and save
        s_adjusted = ''.join(
            ['>'+str(i)+'\n'+s[i] for i in range(len(s))])

        #clear out any previous data
        if os.path.isdir('temp'):
            shutil.rmtree('temp')
        os.mkdir('temp')

        with open('temp/s_adjusted.fa','w') as f:
            f.write(s_adjusted)

        # cluster the sequences
        fastas = clustalo()
        print('clustalo done')
        df = pd.DataFrame(list(map(lambda x:list(
            ''.join(x.split('\n')[1:])),fastas)))

        return df



def get_info(df,name,cutoff):
    #consensus sequence
    df.loc['c'] = df.apply(lambda x:x.value_counts().index[0])
    t = df.T
    d = t.loc[t['c']!='-']
    d.index = range(d.shape[0])
    #identify for each sample those positions where the allele differs
    diffs = pd.Series([d.loc[d[i]!=d['c']].
                       shape[0] for i in range(d.shape[1]-1)],
                      index = d.columns[:-1])
    #take out the outliers those less than 100*(1-cutoff)% sequence id
    d_ordinary = d.drop(diffs[diffs>d.shape[0]*cutoff].index,axis = 1)
    if d_ordinary.shape[1]==1:
        raise Exception('all samples outside cutoff limit')
    snvs = d_ordinary.iloc[:,:-1].apply(snv,axis = 1)
    vc = snvs.value_counts().reindex([False,True]).fillna(0)

    # find the proportion of nucleotides that have significant
    # alternative alleles
    prop_alleles = vc[True]/vc.sum()

    consensus = ''.join(d['c'].values)
    allele_index = snvs[snvs].index
    alleles = d.loc[allele_index]
    valcounts = lambda x:x.value_counts().reindex(list('ACGT')).\
                fillna(0)
    vcs = alleles.iloc[:,:-1].apply(valcounts,axis = 1)
    # only check mutations in 3rd register
    vcs2 = vcs.reindex([i for i in vcs.index if i%3==2])
    silents = vcs2.index.map(lambda i: is_silent(i,vcs,consensus))
    prop_silents = list(silents).count(True)/vcs.index.size
    info = pd.Series([consensus, prop_silents, 
                      prop_alleles,d_ordinary.shape[1]-1])
    info.name = name
    return info



def get_consensus(series,num,cutoff = 0.01):
          
    """given the series of lists of aligned sequences, find
    all the consensus sequences and allele changes.

    Uses 5% frequency as the cutoff for valid alleles."""
    #s is a list of sequences from the bacterial examples
    s = series.iloc[num]
    name = series.index[num]
    #if all the sequences are identical we can bypass this 
    
    if len(set(s))>1:
        df = do_clustalo(s)
        info = get_info(df,name,cutoff)
        return info,df

def analyse(series,bacname,op):
    
    with open('{}/results/{}'.format(bacname,op)) as f:
        sample_num = len(json.load(f)[0])
    information = []
    dfs = {}
    for num in range(series.shape[0]):
        try:
            info,df = get_consensus(series,num,0.01)
            print(info.name)
            information.append(info)
            print(info.iloc[[1,2]])
            dfs[info.name]=df
            information_df = pd.DataFrame(information)
            information_df.columns = ['consensus','proportion_silents',
                        'prop_with_polymorphisms','acceptable_samples']
            information_df['sample_num'] = sample_num
            information_df.to_csv('DDR/{}_{}_information.csv'.
                                  format(op[0],bacname))
        except Exception as e:
            print(e)
            print('{} didnt work'.format(series.index[num]))
    return information, dfs

def cluster_ddr(bacname,op):
    
    series = pd.Series(get_seqs(bacname,op))
    
    information, dfs = analyse(series,bacname,op)

    
class DdrGene:
    
    def clustalo():
        """align all the nucleotide fastas using clustalo"""
        inpath = '/home/skw24/Bacteria/temp/fastas'
        outpath = '/home/skw24/Bacteria/temp/result.fa'
        p = subprocess.Popen(['clustalo','--force','-i',
                              inpath,'-o',outpath])
        p.communicate()
        with open(outpath,'r') as f:
            fastas = f.read().split('>')[1:]
        os.remove(outpath)
        os.remove(inpath)
        return fastas
    
    def __init__(self,gene,bacname,
                 colour,sequences,
                similar = 0.05, snv_cutoff =0.95):
        try:
            self.gene = gene
            self.bacname = bacname
            self.colour = colour
            self.sequences = sequences[gene].dropna()
            self.fasta = ''.join(self.sequences.values)
            self.aligned = self.do_clustalo()
            self.info = self.get_info(similar,snv_cutoff)

            
        except KeyboardInterrupt:
            raise
        except Exception as e:
            print(e)
            raise

    def do_clustalo(self):
        aligned_path = 'DDR/{}_{}_{}_aligned.csv'.format(\
                        self.colour,self.bacname,self.gene)
        if not os.path.isfile(aligned_path):
            #clear out any previous data
            if os.path.isdir('temp'):
                shutil.rmtree('temp')
            os.mkdir('temp')

            with open('temp/fastas','w') as f:
                f.write(self.fasta)
                
            # cluster the sequences
            self.aligned_fastas = DdrGene.clustalo()
            print('clustalo done')
            aligned = pd.DataFrame(list(map(lambda x:list(
                ''.join(x.split('\n')[1:])),self.aligned_fastas)))
            aligned.index = self.sequences.index
            aligned.loc['c'] = aligned.apply(
                lambda x:x.value_counts().index[0])
            aligned.to_csv(aligned_path)
        else:
            aligned = pd.read_csv(aligned_path,
                                  index_col = 0)
        return aligned
    
    
    def get_consensus(self):
        consensus = ''.join(self.aligned.loc['c'].values).replace(
            '-','')
        return '>{}_{}_{}\n'.format(self.gene,
                                    self.colour,
                                    self.bacname)\
                +'\n'.join([consensus[i:i+60] for i in 
                          range(0,len(consensus),60)])\
                +'\n'

    def get_percs_changed(self,similar,snv_cutoff):
        t = self.aligned.T
        d = t.loc[t['c']!='-']

        d.index = range(d.shape[0])
        #identify for each sample those positions where the allele differs
        changes = d.iloc[:,:-1].apply(
            lambda col: (col!=d['c']).value_counts()
            .reindex([True,False]).fillna(0)[True]
            /d.shape[0])
        d =d[changes[changes<similar].index]
        d_vc = d.apply(lambda row: row.value_counts().reindex(
            list('ACGT')).fillna(0).astype(int),axis = 1)
        perc_nucs_changed = d_vc.apply(
            lambda row: (row.max()/row.sum())<snv_cutoff,
            axis =1).sum()/d_vc.shape[0]*100

        translated = d.apply(lambda col: 
                             pd.Series(list(
                                 translate(''.join(list(col))))))

        tprops = translated.apply(lambda row: 
                                  row.value_counts(),axis =1
                                 ).fillna(0)

        aa_prop = tprops.apply(lambda row: 
                               row.max()/row.sum(),axis=1)
        perc_aas_changed = aa_prop[aa_prop<snv_cutoff].shape[0]\
                            /aa_prop.shape[0]*100
        return perc_nucs_changed, perc_aas_changed

    def get_info(self,similar,snv_cutoff):
        self.consensus = self.get_consensus()

        self.nuc_changes,self.aa_changes = self.get_percs_changed(
            similar,snv_cutoff)

        info= pd.Series([self.nuc_changes,
                               self.aa_changes,
                               self.consensus],
                 index = ['perc_nuc_changes',
                          'perc_aa_changes',
                          'consensus_fasta'])
        info.name = self.gene
        return info



def get_sizes(bacname,options):
    '''returns clusters of subspecies from amongst the options with more than
    twenty members.'''
    s = {}
    for op in options:
        with open('{}/results/{}'.format(bacname,op)) as f:
            s[op] = len(json.load(f)[0])
    sizes = pd.Series(s)
    return  sizes[sizes>20]

bacs = ['acinetobacter_baumannii',
        'bacillus_cereus',
 'burkholderia_pseudomallei',
 'clostridioides_difficile',
 'enterococcus_faecalis',
 'enterococcus_faecium',
 'escherichia_coli',
 'klebsiella_pneumoniae',
 'listeria_monocytogenes',
 'mycobacterium_abscessus',
 'mycobacterium_tuberculosis',
 'neisseria_gonorrhoeae',
 'neisseria_meningitidis',
 'pseudomonas aeruginosa',
 'salmonella_enterica',
 'streptococcus pneumoniae',
]





def analyse_interspecies():
    '''This function relies on two pre-cursors. 
    Firstly that the bacterial species have already been clustered 
    into subspecies.
    and secondly that the ddr genes have already been identified.
    It then aligns the genes for each subspecies and identifies the 
    proportion of amino acids and nucleotides that have been changed.
    Together with the consensus sequence for each subspecies this 
    forms the output.'''
    
    
    bad = []
    for bacname in bacs:
        options = [i for i in os.listdir(
            '{}/results'.format(bacname)) 
               if 'shared_clusters_large.json' in i]

        sizes = get_sizes(bacname,options)
        for op in sizes.index:
            try:
                colour = op[0]

                sequences = pd.read_csv('DDR/{}_{}_ddr_sequences.csv'.
                                        format(colour,bacname),index_col = 0)
                information = pd.DataFrame()

                for j,gene in enumerate(ddr_genes):
                    self = DdrGene(gene,bacname,colour,sequences)
                    information = information.append(self.info)
                    information.to_csv('DDR/{}_{}_ddr_consensus.csv'.
                                       format(colour,bacname))
                    print(information.iloc[:,1:])
                    print('{} of {} {}'.format(j+1,len(ddr_genes),gene))
            except KeyboardInterrupt :
                raise
            except Exception as e:
                bad.append('DDR/{}_{}_ddr_sequences.csv'.
                                    format(colour,bacname))
                
def clustalo(data):
    try:
        r = random.randint(100000,999999)
        temp = 'temp{}.fa'.format(str(r))
        temp1 = 'temp{}.fa'.format(str(r+1))
        with open(temp,'w') as f:
                f.write(data)
        p = subprocess.Popen(['clustalo','--force','-i',
                              temp,'-o',temp1])
        p.communicate()
        with open(temp1,'r') as f:
            fastas = f.read().split('>')[1:]
        os.remove(temp)
        os.remove(temp1)
        return fastas

    except (OSError,FileNotFoundError) as e:
        print(e)
        return e

